import styles from "./list.module.scss";
import throttle from "lodash.throttle";
import { useContext, useRef, useState } from "react";
import Row from "../Row/Row";
import ListContext from "../../context/ListContext";

const renderValue = (value: any): string | number => {
  if (typeof value === "boolean") {
    return value ? "true" : "false";
  }
  return value;
};

export default function List() {
  // init
  const {
    viewportHeight,
    amountRows,
    rowHeight,
    scrollTop,
    setScrollTop,
    indexStart,
    indexEnd,
    oncePerMs,
    rowsRef,
    amountRowsBuffered,
  } = useContext(ListContext);

  const values = {
    viewportHeight,
    amountRows,
    rowHeight,
    scrollTop,
    indexStart,
    indexEnd,
    amountRowsBuffered,
    updateFrequency: `${Math.round(1000 / oncePerMs)}x p/sec`,
  };

  // data
  const data: DataItem[] = [];
  for (let index = 0; index < amountRows; index++) {
    data.push({ index, text: `Index ${index}`, top: index * rowHeight });
  }

  const update = throttle((e) => setScrollTop(e.target.scrollTop), oncePerMs, {
    leading: false,
  });

  return (
    <>
      <div
        className={styles.list}
        onScroll={update}
        style={{ height: viewportHeight }}
      >
        <div
          className={styles.rows}
          style={{ height: amountRows * rowHeight }}
          ref={rowsRef}
        >
          {[...data].slice(indexStart, indexEnd + 1).map((item) => (
            <Row key={item.index} style={{ top: item.top }} item={item} />
          ))}
        </div>
      </div>
      <div className={styles.keyValues}>
        {Object.keys(values).map((key) => (
          <div key={key}>
            <span className={styles.valueKey}>{key}:</span>
            {renderValue(values[key])}
          </div>
        ))}
      </div>
    </>
  );
}
